package top.klausmax.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.DubboService;
import top.klausmax.mapper.UserMapper;
import top.klausmax.pojo.MyUser;
import top.klausmax.service.UserService;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 19:47
 */
@DubboService
public class UserServiceImpl extends ServiceImpl<UserMapper, MyUser> implements UserService {
    @Override
    public MyUser findUserByUserName(String userName) {
        QueryWrapper<MyUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name",userName);
        //select * from tb_user where user_name = #{userName}
        return this.getOne(queryWrapper);
    }
}
