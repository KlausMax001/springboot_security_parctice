package top.klausmax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.klausmax.pojo.MyUser;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 19:50
 */
@Repository
public interface UserMapper extends BaseMapper<MyUser> {
}
