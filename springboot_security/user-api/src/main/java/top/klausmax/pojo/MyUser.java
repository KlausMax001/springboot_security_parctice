package top.klausmax.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 19:33
 */

@Data
@TableName("tb_user")
@AllArgsConstructor
@NoArgsConstructor
public class MyUser implements Serializable {

    @TableId(value="id",type = IdType.AUTO)
    private Long id;

    // 用户名
    private String userName;

    // 密码
    private String password;

    // 姓名
    private String name;

    // 年龄
    private Integer age;

    // 性别，1男性，2女性
    private Integer sex;

    // 出生日期
    private String birthday;

    //邮箱
    private String email;

    //权限
    private String security;

}
