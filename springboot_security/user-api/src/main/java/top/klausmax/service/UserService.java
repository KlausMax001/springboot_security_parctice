package top.klausmax.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.klausmax.pojo.MyUser;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 19:34
 */
public interface UserService extends IService<MyUser>{
    MyUser findUserByUserName(String userName);
}
