package top.klausmax.auth;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import top.klausmax.pojo.MyUser;
import top.klausmax.service.UserService;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 22:10
 */
@Component
public class MyUserDetailsService implements UserDetailsService {

    @DubboReference
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        MyUser user = userService.findUserByUserName(userName);
        if(user!= null){
           return new User(userName, user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(user.getSecurity()));
        }
        return null;
    }
}
