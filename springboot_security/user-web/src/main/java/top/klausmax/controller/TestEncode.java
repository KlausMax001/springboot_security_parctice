package top.klausmax.controller;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 22:38
 */
public class TestEncode {
    public static void main(String[] args) {
        BCryptPasswordEncoder en = new BCryptPasswordEncoder();
        String encode = en.encode("123");
        System.out.println("encode = " + encode);
    }
}
