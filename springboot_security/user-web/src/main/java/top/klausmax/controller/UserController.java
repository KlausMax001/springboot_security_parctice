package top.klausmax.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.klausmax.pojo.MyUser;
import top.klausmax.service.UserService;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 20:11
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @DubboReference(check = false)
    private UserService userService;

    @GetMapping("/findAll")
    @PreAuthorize("hasAuthority('admin')")
    public List<MyUser> findAll() {
        return userService.list();
    }



    @GetMapping("/sayHello/{name}")
    public String hi(@PathVariable("name")String name) {
        return "hello:"+name;
    }
}
