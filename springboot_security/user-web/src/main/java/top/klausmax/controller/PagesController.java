package top.klausmax.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 21:29
 */
@RestController
public class PagesController {

    @GetMapping("/index")
    public String index() {

         return "pages/index.html";
    }
}
