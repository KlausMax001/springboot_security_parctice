package top.klausmax.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 21:38
 */
//重载三个security的配置方法
//开启全局方法安全过滤
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////        auth.inMemoryAuthentication().withUser("robin").password("{noop}123")
////                .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"))
////                .and()
////                .withUser("zhangsan").password("{noop}456")
////                .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
//
//    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/*","/js/*");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/user.html") //默认登录成功后的跳转页面
                .and()
                .authorizeRequests()
                //request.getRequestUri()
                // /login /login.html
                .antMatchers("/login*").permitAll()//permitAll 所有匹配到规则的路径都放行
                .anyRequest().authenticated()//authenticated 请求地址需要登入认证
                .and()
                .csrf().disable(); //禁用csrf 如果不禁 过滤在第一层就终止了 无法进入后续过滤链
    }
}
